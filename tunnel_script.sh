#!/usr/bin/env bash

setVariables(){
	ENDPOINT=$(cat /root/scripts/endpoint)
        PORT=443
        PROTO=https
        ID=$(cat /root/scripts/ID)
        STATE=$(curl -k $PROTO://$ENDPOINT:$PORT/$ID/)
}


tunnel(){
  echo ${ID}
        while true; do
        autossh -M 200${ID} -R 172.30.0.50:22${ID}:127.0.0.1:22 -N tunnel@${ENDPOINT} -p12122
        echo $$ > /tmp/tunnel.pid
        done

}

checkSummoning(){

        setVariables
        if [[ $STATE == 1 ]]; then
                echo "Control server returned $STATE"
                tunnel
  	elif [[ $STATE == 2 ]]; then
    		kill -9 $(cat /tmp/tunnel.pid)
    		tunnel
        else
                echo "Control server returned $STATE"
   		exit
        fi

}

DEBUG(){
        DEBUG=1
}

checkSummoning
